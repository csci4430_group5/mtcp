#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "mtcp_server.h"
#include "mtcp_common.h"

#define MTCP_SYN 0
#define MTCP_SYN_ACK 1
#define MTCP_FIN 2
#define MTCP_FIN_ACK 3
#define MTCP_ACK 4
#define MTCP_DATA 5

typedef struct {
	int socket_fd;
	struct sockaddr_in* addr;
	socklen_t addrlen;
}thread_args;

/* ThreadID for Sending Thread and Receiving Thread */
static pthread_t send_thread_pid;
static pthread_t recv_thread_pid;

static pthread_cond_t app_thread_sig = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t app_thread_sig_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t send_thread_sig = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t send_thread_sig_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_mutex_t info_mutex = PTHREAD_MUTEX_INITIALIZER;

static unsigned int ack_no=0;
static int phase=0;//1 when accept() terminates,2 when close() begins
static thread_args* client_arg;

static char* global_buf;
static int begin_index=0;//the index of the begining of remaining data in global recv buffer
static int end_index=-1;//the index of last byte
static int max_file_size=1<<28;
//any operation on global_buf, begin_index, end_index should be atomic

static int read_return_indicator=0;

/* The Sending Thread and Receive Thread Function */
static void *send_thread();
static void *receive_thread();

void mtcp_accept(int socket_fd, struct sockaddr_in *client_addr){
	global_buf=(char*)malloc(sizeof(char)*(max_file_size));
	memset(global_buf,0,max_file_size);
		
	client_arg=(thread_args*)malloc(sizeof(thread_args));
	client_arg->socket_fd=socket_fd;
	client_arg->addr=client_addr;
	client_arg->addrlen=sizeof(struct sockaddr_in);
	pthread_mutex_lock(&app_thread_sig_mutex);
	pthread_create(&send_thread_pid,NULL,send_thread,NULL);
	pthread_create(&recv_thread_pid,NULL,receive_thread,NULL);
	//wait for the handshake to be finished
	pthread_cond_wait(&app_thread_sig,&app_thread_sig_mutex);
	pthread_mutex_unlock(&app_thread_sig_mutex);
	return;
}

int mtcp_read(int socket_fd, unsigned char *buf, int buf_len){//return value needs clarification
	if(read_return_indicator==-1)
		return -1;
	pthread_mutex_lock(&app_thread_sig_mutex);
	pthread_mutex_lock(&info_mutex);
	if(phase==2 && begin_index > end_index)//4 way handshake has been triggered and global buffer is empty
	{
		/*debug code
		printf("Here Begin_index:%d\n",begin_index);
		printf("Here End_index:%d\n",end_index);
		end debug code*/
		pthread_mutex_unlock(&info_mutex);
		pthread_mutex_unlock(&app_thread_sig_mutex);
		return 0;
	}
	else
	{
		/*debug code
		int flag=0;
		int a,b,c,d;
		end debug code*/
		if(begin_index>end_index)//global buffer is empty, block
		{
			/*debug
			flag=1;
			//printf("p4: Begin index:%d\n",begin_index);
			//printf("p4: end index:%d\n",end_index);
			a=begin_index;
			b=end_index;
			end debug code*/
			pthread_mutex_unlock(&info_mutex);
			pthread_cond_wait(&app_thread_sig,&app_thread_sig_mutex);//in phase 1, will be waken only by receive thread
			pthread_mutex_lock(&info_mutex);
			/*debug
			c=begin_index;
			d=end_index;
			//printf("p5: Begin index:%d\n",begin_index);
			//printf("p5: end index:%d\n",end_index);
			end debug code*/
			if(phase==2 && begin_index > end_index)//check again whether 4 way handshake has been triggered and global buffer is empty
			{
				pthread_mutex_unlock(&info_mutex);
				pthread_mutex_unlock(&app_thread_sig_mutex);
				/*debug
				printf("Exit here\n");
				end debug*/
				return 0;
			}
		}

		//Now the global buffer is not empty, so we read them
		if(begin_index>end_index)
		{
			fprintf(stderr,"Global buffer empty.\n");
			exit(1);
		}
		int len=buf_len;
		if(end_index-begin_index+1<len)
			len=end_index-begin_index+1;
		memcpy(buf,&global_buf[begin_index],len);
		begin_index += len;
		/*debug
		if(len==0)
		{
			printf("p4: Begin index:%d\n",a);
			printf("p4: end index:%d\n",b);
			printf("p5: Begin index:%d\n",c);
			printf("p5: end index:%d\n",d);
			printf("p3: flag: %d\n",flag);
			printf("p3: Begin index:%d\n",begin_index);
			printf("p3: end index:%d\n",end_index);
			printf("Here len is 0\n");
		}
		end debug*/
		pthread_mutex_unlock(&info_mutex);
		pthread_mutex_unlock(&app_thread_sig_mutex);
		return len;
	}
}

void mtcp_close(int socket_fd){
	pthread_join(recv_thread_pid,NULL);
	pthread_join(send_thread_pid,NULL);
	free(global_buf);
	free(client_arg);
	close(socket_fd);
	/*debug
	printf("Begin_index:%d\n",begin_index);
	printf("End_index:%d\n",end_index);
	printf("read_return_indicator: %d\n",read_return_indicator);
	end debug*/
	read_return_indicator=-1;
	return;
}

int send_ack(int mtype)//send back ack, syn-ack or fin-ack; whole function critical section
{
	pthread_mutex_lock(&info_mutex);
	unsigned char buf[MAX_BUF_SIZE];
	memset(buf,0,MAX_BUF_SIZE);
	buf[0]=(mtype<<4)+((ack_no>>24)&0xf);
	buf[1]=(ack_no>>16)&0xff;
	buf[2]=(ack_no>>8)&0xff;
	buf[3]=ack_no&0xff;
	if(sendto(client_arg->socket_fd,buf,4,0,
				(struct sockaddr*)(client_arg->addr),client_arg->addrlen)<0)
	{
		perror("sending back ACK");
		exit(1);
	}
	pthread_mutex_unlock(&info_mutex);
	return 0;
}
static void *send_thread(){
	pthread_mutex_lock(&send_thread_sig_mutex);
	pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex);
	usleep(10000);//wait 10ms so that ACK does not goes back before recvfrom()
	send_ack(MTCP_SYN_ACK);
	pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex);

	while(phase==1)//receiving data
	{
		send_ack(MTCP_ACK);
		pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex);
	}
	usleep(10000);//wait 10ms so that ACK does not goes back before recvfrom()
	send_ack(MTCP_FIN_ACK);
	pthread_mutex_unlock(&send_thread_sig_mutex);
	return NULL;
}

static void *receive_thread(){
	unsigned char buf[MAX_BUF_SIZE];
	memset(buf,0,MAX_BUF_SIZE);
	int len=recvfrom(client_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,
			(struct sockaddr*)(client_arg->addr),&(client_arg->addrlen));//fill the address into client_arg->addr
	int mtype=buf[0]/16;
	if(mtype!=MTCP_SYN)
	{
		perror("First SYN ");
		exit(1);
	}
	ack_no++;
	pthread_cond_signal(&send_thread_sig);//sending SYN-ACK
	len=recvfrom(client_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);
	mtype=buf[0]/16;
	if(mtype!=MTCP_ACK)
	{
		perror("First ACK ");
		exit(1);
	}
	phase=1;
	pthread_cond_signal(&app_thread_sig);//terminate mtcp_accept()
	//then monitor socket for data
	while(1)
	{
		len=recvfrom(client_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);
		if(len==-1)
		{
			read_return_indicator=-1;
			continue;
		}
		mtype=buf[0]/16;
		if(mtype!=MTCP_DATA)//receiving FIN
			break;
		unsigned int seq_no=(((buf[0]%16)*256+buf[1])*256+buf[2])*256+buf[3];
		if(seq_no>ack_no)//out-of-order packet
			continue;
		if(seq_no<ack_no)//already acknowledged packet
		{
			send_ack(MTCP_ACK);
			continue;
		}
		pthread_mutex_lock(&info_mutex);
		ack_no=seq_no+(len-4);
		memcpy(&global_buf[end_index+1],&buf[4],len-4);
		end_index += (len - 4);
		pthread_cond_signal(&app_thread_sig);
		pthread_mutex_unlock(&info_mutex);

		pthread_cond_signal(&send_thread_sig);//sending ACK
	}
	if(mtype!=MTCP_FIN)
	{
		perror("Invalid message type");
		exit(1);
	}
	phase=2;
	pthread_cond_signal(&app_thread_sig);
	pthread_mutex_lock(&info_mutex);
	ack_no++;
	pthread_mutex_unlock(&info_mutex);
	pthread_cond_signal(&send_thread_sig);//sending FIN-ACK

	len=recvfrom(client_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);//receiving the last ACK
	mtype=buf[0]/16;
	if(mtype!=MTCP_ACK)
	{
		perror("Last ACK");
		exit(1);
	}
	return NULL;
}
