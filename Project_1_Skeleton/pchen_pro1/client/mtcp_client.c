#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include "mtcp_client.h"
#include "mtcp_common.h"

#define MTCP_SYN 0
#define MTCP_SYN_ACK 1
#define MTCP_FIN 2
#define MTCP_FIN_ACK 3
#define MTCP_ACK 4
#define MTCP_DATA 5

typedef struct {
	int socket_fd;
	struct sockaddr_in* addr;
	socklen_t addrlen;
}thread_args;

/* -------------------- Global Variables -------------------- */

/* ThreadID for Sending Thread and Receiving Thread */
static pthread_t send_thread_pid;
static pthread_t recv_thread_pid;

static pthread_cond_t app_thread_sig = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t app_thread_sig_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t send_thread_sig = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t send_thread_sig_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_mutex_t info_mutex = PTHREAD_MUTEX_INITIALIZER;

static unsigned int seq_no=0;//sequence number
static int phase=0;//1 when connect() terminates, 2 when close() begins
static thread_args* server_arg;

static char *global_buf;
static int begin_index=0;//the index of the begining of remaining data in global send buffer
static int end_index=-1;//the index of last byte
static int max_file_size=1<<28;//2^28 bytes max
static int last_packet_len=0;//when sending a packet, recording its body length(no mtcp header), for update begin_index 
//it's better to make any operation on global_buf, begin_index, end_index and last_packet_len to be atomic

static int write_return_indicator=0;//-1 for situations when mtcp_write() needs to return -1

/* The Sending Thread and Receive Thread Function */
static void *send_thread();
static void *receive_thread();

/* Connect Function Call (mtcp Version) */
void mtcp_connect(int socket_fd, struct sockaddr_in *server_addr){
	global_buf=(char*)malloc(sizeof(char)*(max_file_size));
	memset(global_buf,0,max_file_size);
	server_arg=(thread_args*)malloc(sizeof(thread_args));
	server_arg->socket_fd=socket_fd;
	server_arg->addr=server_addr;
	server_arg->addrlen=sizeof(struct sockaddr_in);

	//set time-out for recvfrom
	struct timeval time_out;
	time_out.tv_sec=1;
	time_out.tv_usec=0;
	if(setsockopt(socket_fd,SOL_SOCKET,SO_RCVTIMEO,&time_out,sizeof(time_out))<0)
	{
		perror("Set recv timeout");
		exit(1);
	}
	//end of time-out set

	pthread_mutex_lock(&app_thread_sig_mutex);
	pthread_create(&recv_thread_pid,NULL,receive_thread,NULL);
	pthread_create(&send_thread_pid,NULL,send_thread,NULL);
	//wait until sending thread sends SYN and ACK to complete the 3-way handshake
	pthread_cond_wait(&app_thread_sig,&app_thread_sig_mutex);
	pthread_mutex_unlock(&app_thread_sig_mutex);
	return;
}

/* Write Function Call (mtcp Version) */
int mtcp_write(int socket_fd, unsigned char *buf, int buf_len){
	if(write_return_indicator==-1)
		return -1;
	if(phase==2)
		return 0;
	//write to the global buffer as a critical section
	pthread_mutex_lock(&info_mutex);
	memcpy(&global_buf[end_index+1],buf,buf_len);
	end_index += buf_len;
	pthread_mutex_unlock(&info_mutex);

	return buf_len;
}

/* Close Function Call (mtcp Version) */
void mtcp_close(int socket_fd){
	//need to clear the global buffer first
	pthread_mutex_lock(&info_mutex);
	while(begin_index<=end_index)
	{
		pthread_mutex_unlock(&info_mutex);
		usleep(100000);//0.1s
		pthread_mutex_lock(&info_mutex);
	}
	pthread_mutex_unlock(&info_mutex);
	//now global buffer is empty

	//now begin to send FIN
	phase=2;
	pthread_cond_signal(&send_thread_sig);//most likely send_thread is sleeping now
	pthread_join(recv_thread_pid,NULL);
	pthread_join(send_thread_pid,NULL);
	free(global_buf);
	free(server_arg);
	close(socket_fd);
	write_return_indicator=-1;
	return;
}

int send_message(int mtype)//whole function as a critical section
{
	pthread_mutex_lock(&info_mutex);
	unsigned char buf[MAX_BUF_SIZE];
	memset(buf,0,MAX_BUF_SIZE);
	//actually the right shift may cause error if the most significant bit of seq_no is 1, but here it's guaranteed to be 0
	buf[0]=(mtype<<4)+((seq_no>>24)&0xf);
	buf[1]=(seq_no>>16)&0xff;
	buf[2]=(seq_no>>8)&0xff;
	buf[3]=seq_no&0xff;
	int len=0;//the streanm length sent by this function call
	if(mtype==MTCP_DATA)
	{
		if(begin_index>end_index)//empty global buffer
		{
			pthread_mutex_unlock(&info_mutex);
			return -1;
		}
		len=1000;
		if(end_index-begin_index+1<len)
			len=end_index-begin_index+1;
		memcpy(&buf[4],&global_buf[begin_index],len);//send up to 1000 bytes, do not use when end_index<begin_index
		last_packet_len=len;//may need re-transmit, so store the len in last_packet_len and don't update begin_index yet
	}
	if(sendto(server_arg->socket_fd,buf,len+4,0,
				(struct sockaddr*)(server_arg->addr),server_arg->addrlen)<0)
	{
		perror("sending message");
		//exit(1);
		write_return_indicator=-1;
	}
	pthread_mutex_unlock(&info_mutex);
	return 0;
}

static void *send_thread(){
	pthread_mutex_lock(&send_thread_sig_mutex);
	send_message(MTCP_SYN);//sending SYN for handshake
	pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex); //waiting for SYN-ACK
	send_message(MTCP_ACK);//sending ACK for handshake
	pthread_cond_signal(&app_thread_sig);//terminate connect()

	phase=1;
	usleep(10000);//wait 10ms for receive_thread to be prepared(entering recvfrom)
	while(phase==1)//enter data transmission phase
	{
		send_message(MTCP_DATA);
		pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex);
	}
	send_message(MTCP_FIN);
	pthread_cond_wait(&send_thread_sig,&send_thread_sig_mutex);//wait for FIN-ACK
	send_message(MTCP_ACK);

	pthread_mutex_unlock(&send_thread_sig_mutex);
	return NULL;
}

static void *receive_thread(){
	unsigned char buf[MAX_BUF_SIZE];
	memset(buf,0,MAX_BUF_SIZE);
	int len=recvfrom(server_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);
	while(len<0)//time out and SYN-ACK doesn't arrive yet
		len=recvfrom(server_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);
	int mtype=buf[0]/16;
	if(mtype!=MTCP_SYN_ACK)
	{
		perror("First SYN-ACK");
		exit(1);
	}
	seq_no++;
	pthread_cond_signal(&send_thread_sig);//For sending ACK

	while(1)
	{
		len=recvfrom(server_arg->socket_fd,buf,MAX_BUF_SIZE-1,0,NULL,NULL);
		if(len<0)//timeout
		{
			printf("seq_no: %d\n",seq_no);
			printf("Time out once\n");
			pthread_cond_signal(&send_thread_sig);
			continue;
		}

		mtype=buf[0]/16;
		unsigned int ack_no=(((buf[0]%16)*256+buf[1])*256+buf[2])*256+buf[3];
		if(ack_no<=seq_no)//already acknowledged packet, ignore
			continue;
		if(mtype==MTCP_FIN_ACK)
			break;
		pthread_mutex_lock(&info_mutex);//send_message() may read seq_no, so here critical section
		begin_index += last_packet_len;//"remove" last packet from global buffer
		seq_no=ack_no;//last packet well received, update seq_no
		pthread_mutex_unlock(&info_mutex);

		pthread_cond_signal(&send_thread_sig);//sending next packet
	}	
	seq_no++;//now sending thread is most likely be sleeping(right after sending FIN), so no need for lock here 
	pthread_cond_signal(&send_thread_sig);//sending last ACK
	return NULL;
}
